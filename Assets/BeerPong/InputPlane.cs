﻿using UnityEngine;

namespace BeerPong
{
    public class InputPlane : MonoBehaviour
    {
        public Ball Ball;

        private void OnMouseDown() => Ball.OnInputMouseDown();

        private void OnMouseDrag() => Ball.OnInputMouseDrag();

        private void OnMouseUp() => Ball.OnInputMouseUp();
    }
}