using System.Collections;
using UnityEngine;

namespace BeerPong
{
    /// <summary>
    /// This class handles the operation of the ball.
    /// All references to the mouse or the mouse cursor are also used for touch controls.
    /// </summary>
    public class Ball : MonoBehaviour
    {
        // CONSTANTS

        private const int HISTORY_COUNT_MIN = 2;
        
        // ENUMS

        private enum HistoryAction
        {
            AppendAlways,
            AppendIfDifferent
        }

        // SERIALIZED FIELDS

        [Header("Drag & Flick")]
        public int HistoryCount = HISTORY_COUNT_MIN;
        public float HistoryDistanceThreshold;
        public float FlickStopTime;
        
        [Header("Throw")]
        public float Lifetime;
        public Vector3 PilotVector;
        public Vector3 FlickFactor;
        public float CupMagnetFactor;

        [Header("Positional Correction")]
        public Vector3 PositionalCorrectionFactor;
        public Transform InputPlaneCenter;

        // PRIVATE INSTANCE FIELDS

        private Camera m_Camera;
        private int m_HistoryIndex; // The current index for writing a new entry.
        private bool m_HistoryWrapped;
        private Vector3 m_InitialPosition;
        private bool m_IsBallThrown;
        private Vector3[] m_PositionHistory;
        private Rigidbody m_Rigidbody;
        private float[] m_TimeHistory;

        // MONOBEHAVIOUR METHODS

        private void Start()
        {
            if (HistoryCount < HISTORY_COUNT_MIN)
                Debug.LogError($"History count must be at least {HISTORY_COUNT_MIN}.");
            
            m_Camera = Camera.main;
            m_InitialPosition = transform.position;
            m_PositionHistory = new Vector3[HistoryCount];
            m_Rigidbody = GetComponent<Rigidbody>();
            m_TimeHistory = new float[HistoryCount];
        }

        // INPUT METHODS

        public void OnInputMouseDown()
        {
            if (m_IsBallThrown) return;
            
            // When initially pressing down on the mouse button, clear the history.
            ClearHistory();

            // Move the ball to the position of the mouse cursor.
            // Always append this to history as the first entry.
            MoveBallToCursor(HistoryAction.AppendAlways);
        }

        public void OnInputMouseDrag()
        {
            if (m_IsBallThrown) return;
            
            // Move the ball along with the position of the mouse cursor while dragging.
            // Append to history only if the position differs enough from the last entry.
            MoveBallToCursor(HistoryAction.AppendIfDifferent);
        }

        public void OnInputMouseUp()
        {
            if (m_IsBallThrown) return;
            
            // When finally letting up off the mouse button,
            // first move the ball to the position of the cursor, and then append history.
            MoveBallToCursor(HistoryAction.AppendAlways);
            
            // If the time between now and the previous history entry exceeds the "FlickStopTime"
            // it is assumed the ball has zero speed at this time and has thus stopped.
            // If so, reset the ball to its initial position and return.
            float timeDelta = GetRecentTime() - GetPreviousTime();
            if (timeDelta >= FlickStopTime)
            {
                MoveBallToInitialPosition();
                return;
            }

            // Calculate the vector from dragging/flicking the ball.
            var vector = GetRecentPosition() - GetOldestPosition();
            float fullTimeDelta = GetRecentTime() - GetOldestTime();
            vector /= fullTimeDelta;
            
            // If the vector is pointing in the proper direction, throw the ball.
            // Otherwise, reset the ball to its initial position.
            if (vector.y > 0)
                ThrowBall(vector);
            else
                MoveBallToInitialPosition();
        }

        // BALL MOVE METHODS

        /// <summary>
        /// Move the ball to the position of the mouse cursor.
        /// </summary>
        private void MoveBallToCursor(HistoryAction historyAction)
        {
            // Get the mouse position.
            var v3Mouse = Input.mousePosition;

            // The mouse position does not contain a Z value, so we need
            // to provide the difference between the camera and the ball.
            v3Mouse.z = m_InitialPosition.z - m_Camera.transform.position.z;

            // Translate this to world space and complete the repositioning.
            var v3World = m_Camera.ScreenToWorldPoint(v3Mouse);
            transform.position = v3World;

            // Finally, append the history as applicable.
            if (historyAction == HistoryAction.AppendIfDifferent)
                AppendHistoryIfDifferent();
            else
                AppendHistory();
            
            // TEMP
            var angle = new Vector3((v3World.y - 0.5f) * 100, 0, v3World.x * 100);
            transform.eulerAngles = angle;
        }

        /// <summary>
        /// Move the ball to its initial position (reset).
        /// </summary>
        private void MoveBallToInitialPosition()
        {
            transform.position = m_InitialPosition;
            
            //TEMP
            transform.eulerAngles = Vector3.zero;
        }

        /// <summary>
        /// Throw the ball.
        /// </summary>
        private void ThrowBall(Vector3 flickVector)
        {
            // Before throwing, we must take the input vector (the flick),
            // and translate it into a fun gameplay vector (the throw).
            var throwVector = PilotVector;
            throwVector.x += flickVector.x * FlickFactor.x;
            throwVector.y += flickVector.y * FlickFactor.y;
            throwVector.z += flickVector.z * FlickFactor.z;
            
            // Apply positional correction if applicable.
            var distFromCenter = GetRecentPosition() - InputPlaneCenter.position;
            throwVector.x += distFromCenter.x * PositionalCorrectionFactor.x;
            throwVector.y += distFromCenter.y * PositionalCorrectionFactor.y;
            throwVector.z += distFromCenter.z * PositionalCorrectionFactor.z;

            // Now throw the ball!
            m_IsBallThrown = true;
            m_Rigidbody.isKinematic = false;
            m_Rigidbody.velocity = throwVector;
            StartCoroutine(ResetBall());
        }

        private IEnumerator ResetBall()
        {
            yield return new WaitForSeconds(Lifetime);
            m_IsBallThrown = false;
            m_Rigidbody.isKinematic = true;
            m_Rigidbody.velocity = Vector3.zero;
            MoveBallToInitialPosition();
        }

        // BALL POSITION HISTORY METHODS

        /// <summary>
        /// Append the current position to the ball's history
        /// if it is different enough (distant enough) from the previous entry.
        /// </summary>
        private void AppendHistoryIfDifferent()
        {
            var curPos = transform.position;
            var prevPos = GetRecentPosition();
            float d = Vector3.Distance(curPos, prevPos);
            if (d >= HistoryDistanceThreshold) AppendHistory();
        }

        /// <summary>
        /// Append the current position and time to the ball's history.
        /// </summary>
        private void AppendHistory()
        {
            // Append the history.
            m_PositionHistory[m_HistoryIndex] = transform.position;
            m_TimeHistory[m_HistoryIndex] = Time.time;

            // Increment the history index, wrapping if needed.
            int newIndex = m_HistoryIndex + 1;
            if (newIndex < HistoryCount)
            {
                m_HistoryIndex = newIndex;
            }
            else
            {
                m_HistoryIndex = 0;
                m_HistoryWrapped = true;
            }
        }

        /// <summary>
        /// Clear the ball's history.
        /// </summary>
        private void ClearHistory()
        {
            for (var i = 0; i < HistoryCount; i++)
            {
                m_PositionHistory[i] = Vector3.zero;
                m_TimeHistory[i] = 0;
            }

            m_HistoryIndex = 0;
            m_HistoryWrapped = false;
        }

        /// <summary>
        /// Get the ball's oldest position.
        /// </summary>
        private Vector3 GetOldestPosition()
        {
            int oldIndex = m_HistoryWrapped ? m_HistoryIndex : 0;
            return m_PositionHistory[oldIndex];
        }

        /// <summary>
        /// Get the ball's oldest time.
        /// </summary>
        private float GetOldestTime()
        {
            int oldIndex = m_HistoryWrapped ? m_HistoryIndex : 0;
            return m_TimeHistory[oldIndex];
        }

        /// <summary>
        /// Get the ball's most recent position.
        /// </summary>
        private Vector3 GetRecentPosition()
        {
            int curIndex = m_HistoryIndex > 0 ? m_HistoryIndex : HistoryCount;
            return m_PositionHistory[curIndex - 1];
        }

        /// <summary>
        /// Get the ball's most recent time.
        /// </summary>
        private float GetRecentTime()
        {
            int curIndex = m_HistoryIndex > 0 ? m_HistoryIndex : HistoryCount;
            return m_TimeHistory[curIndex - 1];
        }

        /// <summary>
        /// Get the ball's previous time.
        /// </summary>
        private float GetPreviousTime()
        {
            switch (m_HistoryIndex)
            {
                case 0:
                    return m_TimeHistory[HistoryCount - 2];
                case 1:
                    return m_TimeHistory[HistoryCount - 1];
                default:
                    return m_TimeHistory[m_HistoryIndex - 2];
            }
        }
    }
}