﻿using UnityEngine;

namespace BeerPong
{
    public class CupMagnet : MonoBehaviour
    {
        private Ball m_Ball;
        private Collider m_BallCollider;
        private Rigidbody m_BallRigidbody;
        private Transform m_BallTransform;

        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("Ball")) return;
            m_Ball = other.GetComponent<Ball>();
            m_BallCollider = other;
            m_BallRigidbody = other.GetComponent<Rigidbody>();
            m_BallTransform = other.transform;
        }

        private void OnTriggerStay(Collider other)
        {
            if (other != m_BallCollider) return;
            var force = (transform.position - m_BallTransform.position) * m_Ball.CupMagnetFactor;
            m_BallRigidbody.AddForce(force);
        }

        private void OnTriggerExit(Collider other)
        {
            if (other != m_BallCollider) return;
            m_Ball = null;
            m_BallCollider = null;
            m_BallRigidbody = null;
            m_BallTransform = null;
        }
    }
}